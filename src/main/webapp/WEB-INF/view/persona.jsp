<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Listado de personas</title>
</head>
<body>
	<h3>Welcome to Spring Boot MVC</h3>

	<h1>Listado de Personas</h1>

        <div style="padding: 10px;"><a href="${pageContext.request.contextPath}/persona/form">Agregar Persona...</a></div>

            <table border="1">
                <tr>
                    <th style="width:  50px;">No</th>
                    <th style="width: 250px;">Nombre</th>
                    <th style="width: 150px;">Apellido</th>
                    <th style="width: 150px;">Apellido 2</th>
                    <th style="width: 150px;">Edad</th>
                </tr>
                <c:forEach var="persona"
                           items="${personaList}"
                           varStatus="status">
                    <tr>
                        <td><b>${status.index + 1}</b></td>
                        <td>${persona.name}</td>
                        <td>${persona.secondName}</td>
                        <td>${persona.lastName}</td>
                        <td>${persona.age}</td>
                    </tr>
                </c:forEach>
</body>
</html>