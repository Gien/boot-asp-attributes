<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Product</title>
    </head>
    <body>
        <h1>Agregar nuevo producto:</h1>
        <mvc:form modelAttribute="Persona" action="create.do">
            <table>
                <tr>
                    <td><mvc:label path="name">Nombre</mvc:label></td>
                    <td><mvc:input path="name"/></td>
                </tr>
                <tr>
                    <td><mvc:label path="secondName">Apellido 1</mvc:label></td>
                    <td><mvc:input path="secondName"/></td>
                </tr>
                <tr>
                    <td><mvc:label path="lastName">Apellido 2</mvc:label></td>
                    <td><mvc:input path="lastName"/></td>
                </tr>
                <tr>
                    <td><mvc:label path="age">Edad</mvc:label></td>
                    <td><mvc:input path="age"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Enviar persona..."/>
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>