package com.boot.jsp.bootaspattributes.api;

import com.boot.jsp.bootaspattributes.dto.PersonDto;
import com.boot.jsp.bootaspattributes.model.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface IPersonController {
    @GetMapping("/persona")
    List<PersonDto> getPersons();

    @PostMapping("/persona")
    String addPerson(@RequestBody Person persona);
}
