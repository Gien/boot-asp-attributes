package com.boot.jsp.bootaspattributes.api.controller;

import com.boot.jsp.bootaspattributes.api.IPersonController;
import com.boot.jsp.bootaspattributes.dto.PersonDto;
import com.boot.jsp.bootaspattributes.model.Person;
import com.boot.jsp.bootaspattributes.service.IPersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class PersonController implements IPersonController {

    private IPersonService personService;

    public PersonController(IPersonService personService){
        this.personService = personService;
    }

    @Override
    public List<PersonDto> getPersons(){
        return personService.getPersons();
    }

    @Override
    public String addPerson(@RequestBody Person person){
        return personService.addPerson(person);
    }
}
