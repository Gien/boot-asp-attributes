package com.boot.jsp.bootaspattributes.api.controller;

import com.boot.jsp.bootaspattributes.api.IPersonController;
import com.boot.jsp.bootaspattributes.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j
public class JspController {

    private IPersonController personController;

    public JspController(final IPersonController personController){
        this.personController = personController;
    }

    @GetMapping({"/", "/persona.do"})
    public String listPerson(Model model){
        model.addAttribute("personaList", personController.getPersons());
        return "persona";
    }


    @GetMapping("/persona/form")
    public String productForm(Model model) {
        model.addAttribute("Persona", new Person());
        return "personaform";
    }


    @PostMapping(value = "/persona/create")
    public String productCreate(@ModelAttribute("Persona") Person persona) {
        log.debug("Registrar persona: {}", persona);
        personController.addPerson(persona);
        return "redirect:/persona.do";
    }
}
