package com.boot.jsp.bootaspattributes.model;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private String name;
    private String secondName;
    private String lastName;
    private String age;

}
