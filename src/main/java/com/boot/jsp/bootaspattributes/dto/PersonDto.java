package com.boot.jsp.bootaspattributes.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PersonDto {
    private String id;

    private String name, secondName, lastName;

    private String age;
}
