package com.boot.jsp.bootaspattributes.service;

import com.boot.jsp.bootaspattributes.dto.PersonDto;
import com.boot.jsp.bootaspattributes.model.Person;

import java.util.List;

public interface IPersonService {
    String addPerson(Person person);

    List<PersonDto> getPersons();

    PersonDto getPerson(String key);
}
