package com.boot.jsp.bootaspattributes.service.impl;

import com.boot.jsp.bootaspattributes.dto.PersonDto;
import com.boot.jsp.bootaspattributes.model.Person;
import com.boot.jsp.bootaspattributes.service.IPersonService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonService implements IPersonService {

    private Map<String, Person> personRepository;

    @Override
    public String addPerson(Person person){
        if (personRepository == null){
            personRepository = new HashMap<>();
        }

        String key = UUID.randomUUID().toString();

        personRepository.put(key, person);

        return key;
    }

    @Override
    public List<PersonDto> getPersons(){
        List<PersonDto> result = new ArrayList<>();
        personRepository.forEach((key, value) ->
                result.add(generatePersonDto(key, value)));


        return result;
    }

    @Override
    public PersonDto getPerson(String key){
        return generatePersonDto(key, personRepository.get(key));
    }

    private PersonDto generatePersonDto(String key, Person person){
       return PersonDto.builder()
               .id(key)
               .name(person.getName())
               .secondName(person.getSecondName())
               .lastName(person.getLastName())
               .age(person.getAge())
               .build();
    }

}
